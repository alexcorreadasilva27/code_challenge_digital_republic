## Nome
Calculadora de tintas

## Descrição
É um projeto que consiste em calcular o quanto de tinta é necessário para pintar quatro parede, onde coloca-se as medidas de cada parede e assim é calculado a quantia de litros totais para pinta-las.

## Badges
[![Netlify Status](https://api.netlify.com/api/v1/badges/8681fad0-e68b-4f5a-b15b-145d3d1f8d7a/deploy-status)](https://app.netlify.com/sites/calculandotinta/deploys)

## Visualização
![](/visual_my_app.gif)

## Instalação
Essa informação se encontra no README que se encontra na pasta my-app
## Para SUPORTE
email: alexcorreas1994@gmail.com

## Autor
@alexcorreadasilva27
