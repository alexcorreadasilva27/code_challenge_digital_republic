## Sobre o projeto
* Projeto é uma calculadora para quanto de tinta precisa para pintar quatro paredes
* Desafio Técnico da Digital Republic
* Mais informações: https://gitlab.com/digitalrepublic/code-challenge

## Como rodar a aplicação
* clone o projeto;
* Abra o terminal e navegue até a pasta /my-app;
* No terminal execute o comando: `npm install` para instalar as dependências;
* Após a instalação das depedências execute o comando `npm start`;
* Abra o browser no endereco: http://localhost:3000
* ou acesse o link: https://calculandotinta.netlify.app/
 [![Netlify Status](https://api.netlify.com/api/v1/badges/8681fad0-e68b-4f5a-b15b-145d3d1f8d7a/deploy-status)](https://app.netlify.com/sites/calculandotinta/deploys)

## informações
* Solução do code challenge da Digital-Republic feito por: Alex Corrêa da Silva
* Disponível online em: https://calculandotinta.netlify.app/
* Código Fonte Disponível em: https://gitlab.com/alexcorreadasilva27/code_challenge_digital_republic/
